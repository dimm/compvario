package ru.skyway.vario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.collections.buffer.CircularFifoBuffer;

import com.androidplot.xy.*;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;

public class MPMotionSensorActivity extends Activity implements SensorEventListener{

	SensorManager sensor_manager;
	Sensor sensor1;
	static CircularFifoBuffer databuf = new CircularFifoBuffer(5000);
	
	private static double vario = Double.NaN;

	private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	private XYPlot mySimpleXYPlot;
	
	private SimpleXYSeries dataSeries = null;
	
	private ConnectedThread conThread = null;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mpmotion_sensor);

        sensor_manager = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensor1 = sensor_manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        setupBt();
        
		sensor_manager.registerListener((SensorEventListener)MPMotionSensorActivity.this, sensor1,SensorManager.SENSOR_DELAY_NORMAL);

        mySimpleXYPlot = (XYPlot) findViewById(R.id.myVarioXYPlot);
        mySimpleXYPlot.setRangeBoundaries(-400, 400, BoundaryMode.FIXED);
        
 
        dataSeries = (SimpleXYSeries)getLastNonConfigurationInstance();
        if(null == dataSeries) {
	        dataSeries = new SimpleXYSeries("vario");
	        dataSeries.useImplicitXVals();
	        
        }
 
        LineAndPointFormatter series1Format = new LineAndPointFormatter(
                Color.rgb(0, 200, 0),                   // line color
                Color.rgb(0, 100, 0),                   // point color
                null,
                new PointLabelFormatter(Color.TRANSPARENT));                                  // fill color (none)
 
        // add a new series' to the xyplot:
        mySimpleXYPlot.addSeries(dataSeries, series1Format);
 
        // reduce the number of range labels
        mySimpleXYPlot.setTicksPerRangeLabel(3);
 
    }
	
    private void setupBt() {
    	Set<BluetoothDevice> set = mBluetoothAdapter.getBondedDevices();
    	BluetoothDevice dev = set.iterator().next();
    	if(dev != null) {
    		ConnectThread thr = new ConnectThread(dev);
    		thr.start();
    	}
    }

	@Override
	protected void onResume() {
		super.onResume();
		databuf.clear();
		sensor_manager.registerListener((SensorEventListener)MPMotionSensorActivity.this, sensor1,SensorManager.SENSOR_DELAY_NORMAL);
		setupBt();
	}

	@Override
	protected void onStop() {
		super.onStop();
		databuf.clear();
		sensor_manager.unregisterListener((SensorEventListener)this, sensor1);
		if(conThread != null) {
			conThread.cancel();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
//		databuf.clear();
//		sensor_manager.unregisterListener((SensorEventListener)this, sensor1);
//		if(conThread != null) {
//			conThread.cancel();
//		}
	}

	/* sensor listener methods */
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		AccelData data = new AccelData();
		data.setData(event.values);
		databuf.add(data);
		
		if(!Double.isNaN(vario)) {
			
			int cur_buf = databuf.size();
			float[] val = new float[3];
			for(int i=0; i<cur_buf; i++){
				AccelData curdata = (AccelData)databuf.remove();
		
				val[0] += curdata.getData()[0];
				val[1] += curdata.getData()[1];
				val[2] += curdata.getData()[2];
			}
			for(int i=0; i<3;i++) // average
				val[i] /= cur_buf;
			
			double e = vario-val[2];
//			if(Math.abs(e)<150) {
//				e = 0;
//			}
//			DecimalFormat df = new DecimalFormat("###.##");
//			ivText.setText("!"+df.format(val[0])+" "+df.format(val[1])+" "+df.format(val[2])+"\n"+df.format(vario));
//			ivTextLarge.setText(df.format(e));
			vario = Double.NaN;

	        // get rid the oldest sample in history:
	        while (dataSeries.size() > 15) {
	            dataSeries.removeFirst();
	        }
	 
	        // add the latest history sample:
	        dataSeries.addLast(null, e);
	 
	        Log.d("debug", dataSeries.size()+"");
	        
	        // redraw the Plots:
	        mySimpleXYPlot.redraw();
		}
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		return dataSeries;
	}
	
    private class AccelData {
    	private float[] data;
		public float[] getData() {
			return data;
		}
		public void setData(float[] data) {
			this.data = data;
		}
    }
    
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
     
        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
     
            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            } catch (IOException e) { }
            mmSocket = tmp;
        }
     
        public void run() {
            // Cancel discovery because it will slow down the connection
            mBluetoothAdapter.cancelDiscovery();
     
            try {
                mmSocket.connect();
            } catch (IOException connectException) {
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                return;
            }
     
            conThread = new ConnectedThread(mmSocket);
            conThread.start();
        }
    }
    
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
     
        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
     
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) { }
     
            mmInStream = tmpIn;
        }
     
        public void run() {
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                	BufferedReader reader = new BufferedReader(new InputStreamReader(mmInStream));
                	String newLine = reader.readLine();
                	String[] newLineSplit = newLine.split(",");
                	if(newLineSplit.length > 0) {
                		try {
                			vario = Double.parseDouble(newLine.split(",")[3]);
                		} catch(Exception ex) {}
                	}
                } catch (IOException e) {
                	Log.d("debug", "Bluetooth thread stopped");
                    break;
                }
            }
        }
     
        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

}